using System;
using NumberSystemService;
using NumberSystemService.Exceptions;
using NUnit.Framework;

namespace NumberSystemTests
{
  [TestFixture]
  public class Tests
  {
    private  INumberService _numberService; 
    [SetUp]
    public void Setup()
    {
      _numberService = new NumberService();
    }

    [Test]
    public void Difference_GivenZero_ShouldReturn5()
    {
      int input = 0;

      var result = _numberService.Difference(input);

      Assert.AreEqual(5, result);
    }

    [Test]
    public void Difference_Given5_ShouldReturn0()
    {
      var input = 5;

      var result = _numberService.Difference(input);

      Assert.AreEqual(0, result);
    }

    [Test]
    public void Difference_Given4_ShouldReturn1()
    {
      //arrange
      var input = 4;
      //
      var result = _numberService.Difference(input);

      Assert.AreEqual(1, result);
    }

    [Test]
    public void Difference_Given6_ShouldThrowException()
    {
      var input = 6;

      Assert.Throws<InvalidInputException> (() => _numberService.Difference(input));
    }

    [Test]
    public void Difference_Given9_ShouldThrowException()
    {
      var input = 6;

      Assert.Throws<InvalidInputException>(() => _numberService.Difference(input));
    }

    [Test]
    public void Difference_Given20_ShouldThrowException()
    {
      var input = 6;

      Assert.Throws<InvalidInputException>(() => _numberService.Difference(input));
    }

    [Test]
    public void Difference_GivenNegetiveNumber_ShouldThrowException()
    {
      var input = -1;

      Assert.Throws<InvalidInputException>(() => _numberService.Difference(input));
    }


  }
}