﻿using System;
using System.Collections.Generic;
using System.Text;
using NumberSystemService;
using NumberSystemService.Exceptions;
using NUnit.Framework;

namespace NumberSystemTests
{
  [TestFixture]
  public class ConversionServiceTests
  {

    private IConversionService _conversionService;
    [SetUp]
    public void Setup()
    {
      _conversionService = new ConversionService();
    }

    [Test]
    public void ConvertStringToInt_GivenFiveString_ShouldReturnIntegerFive()
    {
      var input = "5";
      var expected = 5;
      var result = _conversionService.ConvertStringToInt(input);

      Assert.AreEqual(expected, result);
    }

    [TestCase("#")]
    [TestCase("%")]
    [TestCase("*")]
    public void ConvertStringToInt_GivenSpecialCharacter_ShouldThrowException(string input)
    {
      Assert.Throws<InvalidInputException>(() => _conversionService.ConvertStringToInt(input));
    }

    [TestCase("A")]
    [TestCase("adsa")]
    [TestCase("bxc")]
    public void ConvertStringToInt_GivenCharacters_ShouldThrowException(string input)
    {
      Assert.Throws<InvalidInputException>(() => _conversionService.ConvertStringToInt(input));
    }
  }
}
