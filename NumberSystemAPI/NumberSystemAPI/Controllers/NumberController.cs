﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NumberSystemService;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NumberSystemAPI.Controllers
{
  [Route("api/[controller]")]
  public class NumberController : Controller
  {
    private readonly INumberService _numberService;
    private readonly IConversionService _conversionService;

    public NumberController(INumberService numberService, IConversionService conversionService)
    {
      _numberService = numberService;
      _conversionService = conversionService;
    }
    /// <summary>
    /// Takes a value and perform subtraction calculations and return results.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    // POST api/<controller>
    [HttpPost]
    public int Post(string value)
    {
      int number = _conversionService.ConvertStringToInt(value);
      return _numberService.Difference(number);
    }
  }
}
