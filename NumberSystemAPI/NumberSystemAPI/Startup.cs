using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using NumberSystemAPI.Middleware;
using NumberSystemService;

namespace NumberSystemAPI
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {

      services.AddControllers();

      services.AddCors(x => x.AddPolicy("europolicy", policyBuilder =>
      {
        policyBuilder.AllowAnyHeader()
          .AllowAnyMethod()
          .AllowAnyOrigin();
      }));

      services.AddSwaggerGen(x =>
      {
        x.SwaggerDoc("", new OpenApiInfo()
        {
          Version = "v1",
          Title = "Difference Between 5 and supplied number"
        });
      });

      services.AddTransient<INumberService, NumberService>();
      services.AddTransient<IConversionService, ConversionService>();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseMiddleware(typeof(ErrorHandlingMiddleware));
      app.UseHttpsRedirection();

      app.UseRouting();
      app.UseCors();
      app.UseAuthorization();


      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });

      
      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Number System v1");
      });
    }
  }
}
