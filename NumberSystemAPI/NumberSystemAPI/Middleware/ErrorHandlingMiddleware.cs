﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NumberSystemService.Exceptions;

namespace NumberSystemAPI.Middleware
{
  public class ErrorHandlingMiddleware
  {
    private readonly RequestDelegate next;
    public ErrorHandlingMiddleware(RequestDelegate next)
    {
      this.next = next;
    }

    public async Task Invoke(HttpContext context /* other dependencies */)
    {
      try
      {
        await next(context);
      }
      catch (Exception ex)
      {
        await HandleExceptionAsync(context, ex);
      }
    }

    private static Task HandleExceptionAsync(HttpContext context, Exception ex)
    {
      var code = HttpStatusCode.InternalServerError; // 500 if unexpected
      var message = string.Empty;
      if (ex is InvalidInputException)
      {
        code = HttpStatusCode.BadRequest;
        message = "Invalid Input. Please enter number below 5";
      }

      var result = JsonConvert.SerializeObject(new { error = message});
      context.Response.ContentType = "application/json";
      context.Response.StatusCode = (int)code;
      return context.Response.WriteAsync(result);
    }
  }
}
