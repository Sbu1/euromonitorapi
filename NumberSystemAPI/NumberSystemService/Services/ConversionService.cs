﻿using NumberSystemService.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberSystemService
{
  /// <summary>
  /// A service that perform numbers conversions.
  /// </summary>
  public class ConversionService : IConversionService
  {
    /// <summary>
    /// Converts a string to an integer.
    /// </summary>
    /// <param name="stringNumber"></param>
    /// <returns></returns>
    public int ConvertStringToInt(string stringNumber)
    {
      var converted =int.TryParse(stringNumber, out var number);

      if (converted)
      {
        return number;
      }

      throw new InvalidInputException();
    }
  }
}
