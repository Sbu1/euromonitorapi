﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberSystemService
{
  public interface INumberService
  {
    /// <summary>
    /// Gets a differences between 5 and supplied number.
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    int Difference(int number);
  }
}
