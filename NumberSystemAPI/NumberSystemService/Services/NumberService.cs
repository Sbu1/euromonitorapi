﻿using NumberSystemService.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace NumberSystemService
{
  /// <summary>
  /// A service for numbers calculations.
  /// </summary>
  public class NumberService : INumberService
  {
    /// <summary>
    /// A method that get differences between 5 and a number.
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public int Difference(int number)
    {
      if (number > 5 || number < 0)
      {
        throw new InvalidInputException();
      }

      return 5 - number;
    }
  }
}
