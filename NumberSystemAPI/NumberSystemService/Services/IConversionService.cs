﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberSystemService
{
  public interface IConversionService
  {
    /// <summary>
    /// Converts a string to an integer.
    /// </summary>
    /// <param name="stringNumber"></param>
    /// <returns></returns>
    int ConvertStringToInt(string stringNumber);
  }
}
